package com.placeholder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class PlaceholderTextField extends JTextField {
    private String placeholder;

    public PlaceholderTextField(String text) {
        this.placeholder = text;

        // Define um FocusListener para manipular o placeholder
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // Limpa o texto quando o foco é ganho
                setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {
                // Define o placeholder se o campo estiver vazio
                if (getText().isEmpty()) {
                    setText(placeholder);
                    setForeground(Color.GRAY); // Define a cor do texto como cinza
                }
            }
        });

        // Define o placeholder inicial
        setText(placeholder);
        setForeground(Color.GRAY); // Define a cor do texto como cinza
    }

    // Sobrescreve o método getText para retornar vazio se o texto for igual ao placeholder
    @Override
    public String getText() {
        String text = super.getText();
        return text.equals(placeholder) ? "" : text;
    }
}
