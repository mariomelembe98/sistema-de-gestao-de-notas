package views.components;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import app.controllers.UsuarioController;
import app.models.Usuario;

public class PanelUsuarios extends JPanel {
    private JTextField idField;
    private JTextField nomeField;
    private JTextField nomeUsuarioField;
    private JPasswordField senhaField;
    private JTextField emailField;
    private JCheckBox isAdminCheckBox;
    private JCheckBox statusCheckBox;
    private JTable usuariosTable;
    private DefaultTableModel tableModel;
    private JButton salvarButton;
    private JButton removerButton;
    private JButton atualizarButton;
    private JButton LimparButton;
    private JButton cancelarButton;
    private JButton pesquisarButton;

    private UsuarioController usuarioController;

    public PanelUsuarios(UsuarioController usuarioController) {
        this.usuarioController = usuarioController;
        setBackground(Color.WHITE);
        setLayout(null);

        // Criando o novo JPanel para ocupar a área do panelPrincipal
        JPanel panelHeader = new JPanel(null);
        panelHeader.setBackground(Color.ORANGE);
        panelHeader.setBounds(0, 0, 800, 80); // Define os bounds para ocupar toda a área
        add(panelHeader);

        JPanel panelContent = new JPanel();
        panelContent.setLayout(null);
        panelContent.setBounds(0, 80, 800, 220);

        JLabel textoProfessor = new JLabel("Usuarios");
        textoProfessor.setForeground(Color.WHITE);
        textoProfessor.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textoProfessor.setBounds(50, 10, 800, 90);
        panelHeader.add(textoProfessor);

        JLabel labelTitulo = new JLabel("Usuários");
        labelTitulo.setFont(new Font("Tahoma", Font.PLAIN, 20));
        labelTitulo.setBounds(10, 10, 200, 25);
        add(labelTitulo);

        idField = new JTextField();
        idField.setEditable(false);
        idField.setBounds(20, 150, 50, 25);
        add(idField);

        nomeField = new JTextField();
        nomeField.setBounds(80, 150, 150, 25);
        add(nomeField);

        nomeUsuarioField = new JTextField();
        nomeUsuarioField.setBounds(250, 150, 150, 25);
        add(nomeUsuarioField);

        senhaField = new JPasswordField();
        senhaField.setBounds(420, 150, 150, 25);
        add(senhaField);

        emailField = new JTextField();
        emailField.setBounds(590, 150, 150, 25);
        add(emailField);

        isAdminCheckBox = new JCheckBox("isAdmin");
        isAdminCheckBox.setBounds(20, 200, 80, 25);
        add(isAdminCheckBox);

        statusCheckBox = new JCheckBox("Status");
        statusCheckBox.setBounds(120, 200, 80, 25);
        add(statusCheckBox);

        salvarButton = new JButton("Salvar");
        salvarButton.setBounds(20, 250, 100, 30);
        salvarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adicionarUsuario();
            }
        });

        removerButton = new JButton("Remover");
        removerButton.setBounds(150, 250, 100, 30);
        removerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adicionarUsuario();
            }
        });

        atualizarButton = new JButton("Atualizar");
        atualizarButton.setBounds(280, 250, 100, 30);
        atualizarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adicionarUsuario();
            }
        });

        LimparButton = new JButton("Limpar");
        LimparButton.setBounds(420, 250, 100, 30);
        LimparButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        cancelarButton = new JButton("Cancelar");
        cancelarButton.setBounds(590, 250, 100, 30);
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        pesquisarButton = new JButton("Pesquisar");
        pesquisarButton.setBounds(420, 250, 100, 30);
        pesquisarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        add(salvarButton);
        add(removerButton);
        add(atualizarButton);
        add(LimparButton);
        add(cancelarButton);
        add(pesquisarButton);

        String[] colunas = {"ID", "Nome", "Nome de Usuário", "Senha", "Email", "isAdmin", "Status"};
        tableModel = new DefaultTableModel(colunas, 0);
        usuariosTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(usuariosTable);
        scrollPane.setBounds(20, 300, 720, 200);
        add(scrollPane);

        atualizarTabela();
    }

    private void adicionarUsuario() {
        String nome = nomeField.getText();
        String nomeUsuario = nomeUsuarioField.getText();
        String senha = new String(senhaField.getPassword());
        String email = emailField.getText();
        boolean isAdmin = isAdminCheckBox.isSelected();
        boolean status = statusCheckBox.isSelected();

        Usuario usuario = new Usuario(nome, nomeUsuario, senha, email, isAdmin, status);
        usuarioController.adicionarUsuario(usuario);

        atualizarTabela();
        limparCampos();
    }

    private void limparCampos() {
        nomeField.setText("");
        nomeUsuarioField.setText("");
        senhaField.setText("");
        emailField.setText("");
        isAdminCheckBox.setSelected(false);
        statusCheckBox.setSelected(false);
    }

    private void atualizarTabela() {
        tableModel.setRowCount(0); // Limpa a tabela
        for (Usuario usuario : usuarioController.getUsuarios()) {
            Object[] rowData = {usuario.getId(), usuario.getNome(), usuario.getNomeUsuario(), usuario.getSenha(),
                    usuario.getEmail(), usuario.isAdmin(), usuario.getStatus()};
            tableModel.addRow(rowData);
        }
    }
}
