package views.components;

import javax.swing.*;
import java.awt.*;

public class PanelDisciplinas extends JPanel {
    public PanelDisciplinas() {
        setBackground(Color.WHITE);
        setLayout(null);

        // Criando o novo JPanel para ocupar a área do panelPrincipal
        JPanel panelHeader = new JPanel(null);
        panelHeader.setBackground(Color.ORANGE);
        panelHeader.setBounds(0, 0, 800, 80);

        JLabel textoDisciplina = new JLabel("Disciplina");
        textoDisciplina.setForeground(Color.WHITE);
        textoDisciplina.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textoDisciplina.setBounds(50,10,800,90);
        panelHeader.add(textoDisciplina);

        add(panelHeader);
    }
}


