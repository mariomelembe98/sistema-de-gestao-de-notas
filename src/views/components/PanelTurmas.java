package views.components;

import app.models.Turma;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PanelTurmas extends JPanel {

    private JTextField turmaField;
    private JTextField descricaoField;
    private JComboBox<String> anoComboBox;
    private JComboBox<String> semestreComboBox;
    private JComboBox<String> regimeComboBox;
    private JList<String> professoresList;
    private JTable turmaTable;
    private DefaultTableModel tableModel;
    private JCheckBox ativoCheckBox;

    private File arquivo = new File("turmas.dat"); // Arquivo único para armazenar os dados

    public PanelTurmas() {
        setBackground(Color.WHITE);
        setLayout(null);

        // Criando o novo JPanel para ocupar a área do panelPrincipal
        JPanel panelHeader = new JPanel(null);
        panelHeader.setBackground(Color.ORANGE);
        panelHeader.setBounds(0, 0, 800, 80); // Define os bounds para ocupar toda a área

        JLabel textoTurma = new JLabel("Turmas");
        textoTurma.setForeground(Color.WHITE);
        textoTurma.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textoTurma.setBounds(50, 10, 800, 90);
        panelHeader.add(textoTurma);

        add(panelHeader);
        //
        JPanel panelContent = new JPanel();
        panelContent.setBackground(Color.CYAN);
        panelContent.setLayout(null);
        panelContent.setBounds(0, 80, 800, 200);

        JLabel turmaLabel = new JLabel("Turma:");
        turmaLabel.setBounds(20, 20, 100, 30);
        panelContent.add(turmaLabel);

        turmaField = new JTextField();
        turmaField.setBounds(130, 20, 200, 30);
        panelContent.add(turmaField);

        JLabel descricaoLabel = new JLabel("Descrição:");
        descricaoLabel.setBounds(20, 60, 100, 30);
        panelContent.add(descricaoLabel);

        descricaoField = new JTextField();
        descricaoField.setBounds(130, 60, 200, 30);
        panelContent.add(descricaoField);

        JLabel anoLabel = new JLabel("Ano:");
        anoLabel.setBounds(20, 100, 100, 30);
        panelContent.add(anoLabel);

        String[] anos = {"1º Ano", "2º Ano", "3º Ano", "4º Ano"}; // Exemplo de anos
        anoComboBox = new JComboBox<>(anos);
        anoComboBox.setBounds(130, 100, 200, 30);
        panelContent.add(anoComboBox);

        JLabel semestreLabel = new JLabel("Semestre:");
        semestreLabel.setBounds(20, 140, 100, 30);
        panelContent.add(semestreLabel);

        semestreComboBox = new JComboBox<>(new String[]{"1º Semestre", "2º Semestre"});
        semestreComboBox.setBounds(130, 140, 200, 30);
        panelContent.add(semestreComboBox);

        JLabel professoresLabel = new JLabel("Professores:");
        professoresLabel.setBounds(400, 20, 100, 30);
        panelContent.add(professoresLabel);

        professoresList = new JList<>();
        professoresList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane professoresScrollPane = new JScrollPane(professoresList);
        professoresScrollPane.setBounds(500, 20, 200, 80);
        panelContent.add(professoresScrollPane);

        JLabel regimeLabel = new JLabel("Regime:");
        regimeLabel.setBounds(400, 105, 100, 30);
        panelContent.add(regimeLabel);

        regimeComboBox = new JComboBox<>(new String[]{"Laboral", "Pos-Laboral"});
        regimeComboBox.setBounds(500, 105, 200, 30);
        panelContent.add(regimeComboBox);

        JLabel statusLabel = new JLabel("Status:");
        statusLabel.setBounds(400, 140, 100, 30);
        panelContent.add(statusLabel);

        ativoCheckBox = new JCheckBox("Ativo");
        ativoCheckBox.setSelected(true);
        ativoCheckBox.setBounds(500, 140, 80, 30);
        panelContent.add(ativoCheckBox);


        add(panelContent);

        JSeparator separatorAbove = new JSeparator(SwingConstants.HORIZONTAL);
        separatorAbove.setBounds(0, 280, 800, 10); // Definindo a posição e o tamanho
        add(separatorAbove);

        JButton guardarButton = new JButton("Guardar");
        guardarButton.setBounds(100, 285, 100, 30);
        add(guardarButton);

        JButton actualizarButton = new JButton("Actualizar");
        actualizarButton.setBounds(250, 285, 100, 30);
        add(actualizarButton);

        JButton eliminarButton = new JButton("Eliminar");
        eliminarButton.setBounds(400, 285, 100, 30);
        add(eliminarButton);

        JButton limparButton = new JButton("Limpar");
        limparButton.setBounds(550, 285, 100, 30);
        add(limparButton);

        JSeparator separatorDown = new JSeparator(SwingConstants.HORIZONTAL);
        separatorDown.setBounds(0, 320, 800, 10); // Definindo a posição e o tamanho
        add(separatorDown);

        // Tabela de Turmas
        String[] colunas = {"Turma", "Descrição", "Ano", "Semestre", "Professores", "Regime", "Status"};
        tableModel = new DefaultTableModel(colunas, 0);
        turmaTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(turmaTable);
        scrollPane.setBounds(0, 330, 800, 200);
        add(scrollPane);

        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarTurma();
            }
        });

        limparButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limparCampos();
            }
        });

        // Carregar os dados do arquivo ao abrir a tela
        carregarDadosDoArquivo();

        // Adiciona um ouvinte de mouse na tabela para permitir a seleção de linhas
        turmaTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = turmaTable.getSelectedRow();
                if (row != -1) {
                    setCamposComDadosDaLinhaSelecionada(row);
                }
            }
        });

        actualizarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                atualizarTurma();
            }
        });
    }

    // Método para preencher os campos com os dados da linha selecionada na tabela
    private void setCamposComDadosDaLinhaSelecionada(int rowIndex) {
        turmaField.setText((String) turmaTable.getValueAt(rowIndex, 0));
        descricaoField.setText((String) turmaTable.getValueAt(rowIndex, 1));
        anoComboBox.setSelectedItem((String) turmaTable.getValueAt(rowIndex, 2));
        semestreComboBox.setSelectedItem((String) turmaTable.getValueAt(rowIndex, 3));
        // Outros campos
    }

    private void atualizarTurma() {
        // Atualiza os dados na tabela
        int row = turmaTable.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(this, "Selecione uma turma para atualizar!", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String turma = turmaField.getText().trim();
        String descricao = descricaoField.getText().trim();
        String ano = (String) anoComboBox.getSelectedItem();
        String semestre = (String) semestreComboBox.getSelectedItem();
        List<String> professores = professoresList.getSelectedValuesList();
        String regime = (String) regimeComboBox.getSelectedItem();
        boolean status = ativoCheckBox.isSelected();

        // Verificar se algum campo está vazio
        if (turma.isEmpty() || descricao.isEmpty() || ano == null || semestre == null) {
            JOptionPane.showMessageDialog(this, "Por favor, preencha todos os campos!", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String statusText = status ? "Ativo" : "Inativo";

        // Atualiza a linha na tabela com os novos valores
        turmaTable.setValueAt(turma, row, 0);
        turmaTable.setValueAt(descricao, row, 1);
        turmaTable.setValueAt(ano, row, 2);
        turmaTable.setValueAt(semestre, row, 3);
        turmaTable.setValueAt(String.join(", ", professores), row, 4);
        turmaTable.setValueAt(regime, row, 5);
        turmaTable.setValueAt(statusText, row, 6);

        // Limpa os campos
        limparCampos();
    }

    public void setTurma(Turma turma) {
        turmaField.setText(turma.getTurma());
        descricaoField.setText(turma.getDescricao());
        anoComboBox.setSelectedItem(String.valueOf(turma.getAno()));
        semestreComboBox.setSelectedItem(turma.getSemestre());
        List<String> professores = turma.getProfessores();
        regimeComboBox.setSelectedItem(String.valueOf(turma.getRegime()));
        ativoCheckBox.setSelected(turma.isStatus());
        String[] professoresArray = professores.toArray(new String[professores.size()]);
        professoresList.setListData(professoresArray);
    }

    private void guardarTurma() {
        String turma = turmaField.getText().trim();
        String descricao = descricaoField.getText().trim();
        String ano = (String) anoComboBox.getSelectedItem();
        String semestre = (String) semestreComboBox.getSelectedItem();
        List<String> professores = professoresList.getSelectedValuesList();
        String regime = (String) regimeComboBox.getSelectedItem();
        boolean status = ativoCheckBox.isSelected();

        // Verificar se algum campo está vazio
        if (turma.isEmpty() || descricao.isEmpty() || ano == null || semestre == null) {
            JOptionPane.showMessageDialog(this, "Por favor, preencha todos os campos!", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Adiciona os dados na tabela
        String statusText = status ? "Ativo" : "Inativo"; // Converte o status para texto
        String[] rowData = {turma, descricao, ano, semestre, String.join(", ", professores), regime, statusText};
        tableModel.addRow(rowData);

        // Salva os dados no arquivo
        salvarDadosNoArquivo();

        // Limpa os campos
        limparCampos();

        try (FileWriter writer = new FileWriter(arquivo, true)) { // O segundo parâmetro true indica que iremos adicionar ao arquivo
            writer.write("Turma: " + turma + "\n");
            writer.write("Descrição: " + descricao + "\n");
            writer.write("Ano: " + ano + "\n");
            writer.write("Semestre: " + semestre + "\n");
            writer.write("Professores:\n");
            for (String professor : professores) {
                writer.write("- " + professor + "\n");
            }
            writer.write("Regime: " + regime + "\n");
            writer.write("Status: " + statusText + "\n");
            writer.write("\n"); // Adiciona uma linha em branco para separar as turmas
            JOptionPane.showMessageDialog(this, "Turma guardada com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erro ao guardar a turma!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void salvarDadosNoArquivo() {
        try (PrintWriter writer = new PrintWriter(new FileWriter(arquivo))) {
            for (int i = 0; i < turmaTable.getRowCount(); i++) {
                String turma = (String) turmaTable.getValueAt(i, 0);
                String descricao = (String) turmaTable.getValueAt(i, 1);
                String ano = (String) turmaTable.getValueAt(i, 2);
                String semestre = (String) turmaTable.getValueAt(i, 3);
                String professores = (String) turmaTable.getValueAt(i, 4);
                String regime = (String) turmaTable.getValueAt(i, 5);
                String status = (String) turmaTable.getValueAt(i, 6);

                writer.printf("%s;%s;%s;%s;%s;%s;%s\n", turma, descricao, ano, semestre, professores, regime, status);
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erro ao salvar os dados no arquivo!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void limparCampos() {
        turmaField.setText("");
        descricaoField.setText("");
        anoComboBox.setSelectedIndex(0);
        semestreComboBox.setSelectedIndex(0);
        professoresList.clearSelection();
        regimeComboBox.setSelectedIndex(0);
        ativoCheckBox.setSelected(true);
    }

    private void carregarDadosDoArquivo() {
        if (!arquivo.exists()) {
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(arquivo))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(";");
                if (parts.length >= 7) { // Verifica se a linha possui pelo menos 7 elementos
                    Object[] rowData = {parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6]};
                    tableModel.addRow(rowData);
                } else {
                    // Se a linha não tiver elementos suficientes, pule para a próxima
                    System.err.println("Linha inválida no arquivo: " + line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erro ao carregar os dados do arquivo!", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }


}
