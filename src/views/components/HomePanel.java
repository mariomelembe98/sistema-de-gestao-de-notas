package views.components;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 06/05/2024.
 */

public class HomePanel extends JFrame{

    private JTable tabelaEstudantes;
    private List<String> estudantes;

    public HomePanel() {
        // Define o layout da TelaEstudantes como BorderLayout
        setLayout(new BorderLayout());

        // Inicializa a lista de estudantes
        estudantes = new ArrayList<>();

        // Cria uma tabela para exibir os estudantes
        tabelaEstudantes = new JTable(new DefaultTableModel(new Object[]{"ID", "Nome"}, 0));
        JScrollPane scrollPane = new JScrollPane(tabelaEstudantes);

        // Adiciona a tabela em um JScrollPane para permitir rolagem
        add(scrollPane, BorderLayout.CENTER);

        // Adiciona alguns estudantes fictícios à tabela (apenas para exemplo)
        adicionarEstudante("1", "João");
        adicionarEstudante("2", "Maria");
    }

    public void adicionarEstudante(String id, String nome) {
        // Adiciona um novo estudante à lista e à tabela
        estudantes.add(nome);
        DefaultTableModel model = (DefaultTableModel) tabelaEstudantes.getModel();
        model.addRow(new Object[]{id, nome});
    }
}
