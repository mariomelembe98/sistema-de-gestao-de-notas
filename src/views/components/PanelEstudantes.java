package views.components;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class PanelEstudantes extends JPanel {
    private JTextField nomeField;
    private JTextField apelidoField;
    private JTextField dataNascimentoField;
    private JComboBox<String> sexoComboBox;
    private JTextField emailField;
    private JTextField telefoneField;
    private JTextField enderecoField;
    private JComboBox<String> nacionalidadeComboBox;
    private JComboBox<String> provinciaComboBox;
    private JCheckBox statusCheckBox;
    private JCheckBox isAdminCheckBox;
    private JTable estudantesTable;
    private DefaultTableModel tableModel;

    public PanelEstudantes() {
        setBackground(Color.WHITE);
        setLayout(null);

        JPanel panelHeader = new JPanel(null);
        panelHeader.setBackground(Color.ORANGE);
        panelHeader.setBounds(0, 0, 800, 80);
        add(panelHeader);

        JPanel panelContent = new JPanel();
        panelContent.setLayout(null);
        panelContent.setBounds(0, 80, 800, 220);

        JLabel textoEstudante = new JLabel("Estudantes");
        textoEstudante.setForeground(Color.WHITE);
        textoEstudante.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textoEstudante.setBounds(50, 10, 800, 90);
        panelHeader.add(textoEstudante);

        JLabel nomeLabel = new JLabel("Nome:");
        nomeLabel.setBounds(20, 20, 100, 30);
        panelContent.add(nomeLabel);

        nomeField = new JTextField();
        nomeField.setBounds(130, 20, 150, 30);
        panelContent.add(nomeField);

        JLabel apelidoLabel = new JLabel("Apelido:");
        apelidoLabel.setBounds(20, 60, 100, 30);
        panelContent.add(apelidoLabel);

        apelidoField = new JTextField();
        apelidoField.setBounds(130, 60, 150, 30);
        panelContent.add(apelidoField);

        JLabel dataNascimentoLabel = new JLabel("Data de Nascimento:");
        dataNascimentoLabel.setBounds(20, 100, 150, 30);
        panelContent.add(dataNascimentoLabel);

        dataNascimentoField = new JTextField();
        dataNascimentoField.setBounds(150, 100, 130, 30);
        panelContent.add(dataNascimentoField);

        JLabel sexoLabel = new JLabel("Sexo:");
        sexoLabel.setBounds(20, 140, 100, 30);
        panelContent.add(sexoLabel);

        sexoComboBox = new JComboBox<>(new String[]{"Masculino", "Feminino"});
        sexoComboBox.setBounds(130, 140, 150, 30);
        panelContent.add(sexoComboBox);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(20, 180, 100, 30);
        panelContent.add(emailLabel);

        emailField = new JTextField();
        emailField.setBounds(130, 180, 150, 30);
        panelContent.add(emailField);

        JLabel telefoneLabel = new JLabel("Telefone:");
        telefoneLabel.setBounds(300, 20, 100, 30);
        panelContent.add(telefoneLabel);

        telefoneField = new JTextField();
        telefoneField.setBounds(400, 20, 150, 30);
        panelContent.add(telefoneField);

        JLabel enderecoLabel = new JLabel("Endereço:");
        enderecoLabel.setBounds(300, 60, 100, 30);
        panelContent.add(enderecoLabel);

        enderecoField = new JTextField();
        enderecoField.setBounds(400, 60, 150, 30);
        panelContent.add(enderecoField);

        JLabel nacionalidadeLabel = new JLabel("Nacionalidade:");
        nacionalidadeLabel.setBounds(300, 100, 100, 30);
        panelContent.add(nacionalidadeLabel);

        nacionalidadeComboBox = new JComboBox<>(new String[]{"Angolano", "Brasileiro", "Português", "Outro"});
        nacionalidadeComboBox.setBounds(400, 100, 150, 30);
        panelContent.add(nacionalidadeComboBox);

        JLabel provinciaLabel = new JLabel("Província:");
        provinciaLabel.setBounds(300, 140, 130, 30);
        panelContent.add(provinciaLabel);

        provinciaComboBox = new JComboBox<>(new String[]{"Luanda", "Benguela", "Huambo", "Outra"});
        provinciaComboBox.setBounds(400, 140, 150, 30);
        panelContent.add(provinciaComboBox);

        JLabel statusLabel = new JLabel("Status:");
        statusLabel.setBounds(300, 180, 100, 30);
        panelContent.add(statusLabel);

        statusCheckBox = new JCheckBox("Ativo");
        statusCheckBox.setSelected(true);
        statusCheckBox.setBounds(400, 180, 80, 30);
        panelContent.add(statusCheckBox);

        JLabel isAdminLabel = new JLabel("isAdmin:");
        isAdminLabel.setBounds(570, 20, 100, 30);
        panelContent.add(isAdminLabel);

        isAdminCheckBox = new JCheckBox("Sim");
        isAdminCheckBox.setBounds(650, 20, 80, 30);
        panelContent.add(isAdminCheckBox);

        add(panelContent);

        // Adding the table
        String[] colunas = {"Nome", "Apelido", "Data Nascimento", "Sexo", "Email", "Telefone", "Endereço", "Nacionalidade", "Província", "Status", "isAdmin"};
        tableModel = new DefaultTableModel(colunas, 0);
        estudantesTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(estudantesTable);
        scrollPane.setBounds(0, 340, 800, 200);
        add(scrollPane);

        JSeparator separatorAbove = new JSeparator(SwingConstants.HORIZONTAL);
        separatorAbove.setBounds(0, 300, 800, 10);
        add(separatorAbove);

        JButton guardarButton = new JButton("Guardar");
        guardarButton.setBounds(100, 305, 100, 30);
        add(guardarButton);

        JButton actualizarButton = new JButton("Actualizar");
        actualizarButton.setBounds(250, 305, 100, 30);
        add(actualizarButton);

        JButton eliminarButton = new JButton("Eliminar");
        eliminarButton.setBounds(400, 305, 100, 30);
        add(eliminarButton);

        JButton limparButton = new JButton("Limpar");
        limparButton.setBounds(550, 305, 100, 30);
        add(limparButton);

        JSeparator separatorDown = new JSeparator(SwingConstants.HORIZONTAL);
        separatorDown.setBounds(0, 340, 800, 10);
        add(separatorDown);
    }
}
