package views;

import app.controllers.LoginController;
import app.models.Usuario;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TelaLogin extends JFrame {
    private JTextField campoUsuario;
    private JPasswordField campoSenha;
    private JButton botaoLogin;
    private LoginController controller;

    public TelaLogin(LoginController controller) {
        controller.setView(this);
        this.controller = controller;

        // Configurações da janela
        setTitle("Login");
        setSize(700, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setLocationRelativeTo(null);
        setResizable(false);

        JPanel panel = new JPanel();
        panel.setBounds(5,5,300,355);
        panel.setBackground(Color.darkGray);
        add(panel);

        // Componentes da interface
        JLabel labelUsuario = new JLabel("Nome de usuário:");
        labelUsuario.setBounds(390, 125, 120, 25);
        add(labelUsuario);

        campoUsuario = new JTextField(20);
        campoUsuario.setBounds(390, 150, 200, 30);
        add(campoUsuario);

        JLabel labelSenha = new JLabel("Senha:");
        labelSenha.setBounds(390, 180, 80, 25);
        add(labelSenha);

        campoSenha = new JPasswordField(20);
        campoSenha.setBounds(390, 205, 200, 30);
        add(campoSenha);

        botaoLogin = new JButton("Login");
        botaoLogin.setBounds(390, 260, 200, 30);
        botaoLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.realizarLogin();
            }
        });
        add(botaoLogin);
    }

    public String getUsuario() {
        return campoUsuario.getText();
    }

    public String getSenha() {
        return new String(campoSenha.getPassword());
    }

    public void exibirMensagemErro(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
    }

    public void limparCampos() {
        campoUsuario.setText("");
        campoSenha.setText("");
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Usuario modelo = new Usuario();
            LoginController controller = new LoginController(modelo);
            TelaLogin tela = new TelaLogin(controller);
            tela.setVisible(true);
        });
    }
}
