package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TelaRespostas extends JFrame {
    private JTextField[] camposNumeros;
    private JTextField[] camposRespostas;
    private JButton btnCorrigir;

    public TelaRespostas(String gabarito) {
        setTitle("Inserir Respostas dos Alunos");
        setBounds(100, 100, 400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabel lblGabarito = new JLabel("Gabarito: " + gabarito);
        lblGabarito.setBounds(20, 20, 300, 20);
        getContentPane().add(lblGabarito);

        JLabel lblAlunos = new JLabel("Respostas dos Alunos:");
        lblAlunos.setBounds(20, 50, 200, 20);
        getContentPane().add(lblAlunos);

        camposNumeros = new JTextField[10];
        camposRespostas = new JTextField[10];
        for (int i = 0; i < 10; i++) {
            camposNumeros[i] = new JTextField();
            camposNumeros[i].setBounds(20, 80 + 25 * i, 50, 20);
            getContentPane().add(camposNumeros[i]);

            camposRespostas[i] = new JTextField();
            camposRespostas[i].setBounds(80, 80 + 25 * i, 200, 20);
            getContentPane().add(camposRespostas[i]);
        }

        btnCorrigir = new JButton("Corrigir");
        btnCorrigir.setBounds(150, 250, 100, 30);
        getContentPane().add(btnCorrigir);

        btnCorrigir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Aqui você pode corrigir as respostas dos alunos
                StringBuilder respostasAlunos = new StringBuilder();
                for (int i = 0; i < 10; i++) {
                    respostasAlunos.append(camposRespostas[i].getText().toUpperCase());
                }
                JOptionPane.showMessageDialog(null, "Respostas dos alunos:\n" + respostasAlunos);
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                TelaRespostas frame = new TelaRespostas("ABCDEEDCBA");
                frame.setVisible(true);
            }
        });
    }
}
