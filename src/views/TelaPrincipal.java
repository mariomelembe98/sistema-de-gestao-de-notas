package views;

import app.controllers.LoginController;
import app.controllers.UsuarioController;
import views.components.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TelaPrincipal extends JFrame {
    private JLabel labelBemVindo;
    private String usuarioLogado;
    private JButton botaoUsuario;
    private JButton botaoProfessor;
    private JButton botaoEstudante;
    private JButton botaoTurma;
    private JButton botaoDisciplina;
    private JButton botaoCurso;
    private JButton botaoEstatistica;
    private JButton botaoSair;
    private LoginController loginController;

    public TelaPrincipal(UsuarioController usuarioController) {
        this.usuarioLogado = usuarioLogado;

        setTitle("Tela Principal");
        setSize(1030, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setLocationRelativeTo(null);
        setResizable(false);

        //Painel de Cima
        JPanel panelDeCima = new JPanel(null);
        panelDeCima.setBounds(220,0,800,40);
        panelDeCima.setBackground(Color.darkGray);


        labelBemVindo = new JLabel("Ola, " + usuarioLogado + "!");
        labelBemVindo.setForeground(Color.WHITE);
        Font fonte = new Font(labelBemVindo.getFont().getName(), Font.PLAIN, 15);
        labelBemVindo.setFont(fonte);
        labelBemVindo.setSize(300,200);
        labelBemVindo.setBounds(650, 10, 200, 25);
        panelDeCima.add(labelBemVindo);
        add(panelDeCima);

        /*Painel Principal */
        JPanel panelPrincipal = new JPanel(null);
        panelPrincipal.setBounds(220,10,800,560);
        panelPrincipal.setBackground(Color.WHITE);
        add(panelPrincipal);

        /*Painel esquerdo */
        JPanel panel = new JPanel(null);
        panel.setBounds(0,0,220,570);
        panel.setBackground(Color.darkGray);

        // Adicionando uma linha de divisão
        JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
        separator.setBounds(10, 40, 200, 10); // Definindo a posição e o tamanho
        panel.add(separator);

        botaoUsuario = new JButton("Usuarios");
        Font FontButton = new Font(botaoUsuario.getFont().getName(), Font.PLAIN, 15);
        botaoUsuario.setFont(FontButton);
        //botaoUsuario.setSize(300,200);
        botaoUsuario.setBounds(10, 50, 200, 50);
        botaoUsuario.setBackground(Color.WHITE);

        botaoProfessor = new JButton("Professores");
        botaoProfessor.setBounds(10, 100, 200, 50);
        botaoProfessor.setBackground(Color.WHITE);


        botaoEstudante = new JButton("Estudantes");
        botaoEstudante.setBounds(10, 150, 200, 50);
        botaoEstudante.setBackground(Color.WHITE);

        botaoTurma = new JButton("Turmas");
        botaoTurma.setBounds(10, 200, 200, 50);
        botaoTurma.setBackground(Color.WHITE);

        botaoDisciplina = new JButton("Disciplinas");
        botaoDisciplina.setBounds(10, 250, 200, 50);
        botaoDisciplina.setBackground(Color.WHITE);

        botaoCurso = new JButton("Cursos");
        botaoCurso.setBounds(10, 300, 200, 50);
        botaoCurso.setBackground(Color.WHITE);

        botaoEstatistica =  new JButton("Estatisticas");
        botaoEstatistica.setBounds(10, 350, 200, 50);
        botaoEstatistica.setBackground(Color.WHITE);

        botaoSair = new JButton("Sair");
        botaoSair.setBounds(10, 500, 200, 35);
        botaoSair.setBackground(Color.RED);

        panel.add(botaoUsuario);
        panel.add(botaoProfessor);
        panel.add(botaoEstudante);
        panel.add(botaoTurma);
        panel.add(botaoDisciplina);
        panel.add(botaoEstatistica);
        panel.add(botaoCurso);
        panel.add(botaoSair);

        add(panel);

        botaoUsuario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Removendo componentes do panelPrincipal
                panelPrincipal.removeAll();

                // Adicionando o PanelUsuarios ao panelPrincipal com setBounds
                PanelUsuarios panelUsuarios = new PanelUsuarios(usuarioController);
                panelUsuarios.setBounds(0, 0, 800, 500);
                panelPrincipal.add(panelUsuarios);

                // Atualizando a tela
                revalidate();
                repaint();
            }
        });

        botaoProfessor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                panelPrincipal.removeAll();
                PanelProfessores panelProfessores = new PanelProfessores();
                panelProfessores.setBounds(0, 0, 800, 500);
                panelPrincipal.add(panelProfessores);
                revalidate();
                repaint();
            }
        });

        botaoEstudante.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                panelPrincipal.removeAll();
                PanelEstudantes panelEstudantes = new PanelEstudantes();
                panelEstudantes.setBounds(0, 0, 800, 500);
                panelPrincipal.add(panelEstudantes);
                revalidate();
                repaint();
            }
        });

        botaoTurma.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                panelPrincipal.removeAll();
                PanelTurmas panelTurmas = new PanelTurmas();
                panelTurmas.setBounds(0, 0, 800, 550);
                panelPrincipal.add(panelTurmas);
                revalidate();
                repaint();
            }
        });

        botaoDisciplina.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                panelPrincipal.removeAll();
                PanelDisciplinas panelDisciplinas  = new PanelDisciplinas();
                panelDisciplinas.setBounds(0, 0, 800, 500);
                panelPrincipal.add(panelDisciplinas);
                revalidate();
                repaint();
            }
        });


        botaoSair.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int confirmacao = JOptionPane.showConfirmDialog(null, "Deseja realmente sair?", "Sair", JOptionPane.YES_NO_OPTION);
                if (confirmacao == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });


    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            // Crie uma instância de UsuarioController
            UsuarioController usuarioController = new UsuarioController();

            // Crie uma instância de TelaPrincipal passando o UsuarioController
            TelaPrincipal telaPrincipal = new TelaPrincipal(usuarioController);

            // Torna a tela visível
            telaPrincipal.setVisible(true);
        });
    }

}
