package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TelaGabarito extends JFrame {
    private JTextField[] camposRespostas;
    private JButton btnConfirmar;

    public TelaGabarito() {
        setTitle("Inserir Gabarito");
        setBounds(100, 100, 400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        JLabel lblGabarito = new JLabel("Gabarito:");
        lblGabarito.setBounds(20, 20, 100, 20);
        getContentPane().add(lblGabarito);

        camposRespostas = new JTextField[10];
        for (int i = 0; i < 10; i++) {
            camposRespostas[i] = new JTextField();
            camposRespostas[i].setBounds(120 + 30 * i, 20, 20, 20);
            getContentPane().add(camposRespostas[i]);
        }

        btnConfirmar = new JButton("Confirmar");
        btnConfirmar.setBounds(150, 70, 100, 30);
        getContentPane().add(btnConfirmar);

        btnConfirmar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String gabarito = "";
                for (int i = 0; i < 10; i++) {
                    gabarito += camposRespostas[i].getText().toUpperCase();
                }
                JOptionPane.showMessageDialog(null, "Gabarito inserido com sucesso:\n" + gabarito);
                // Aqui você pode chamar a próxima tela passando o gabarito como parâmetro
                // Exemplo: new TelaRespostas(gabarito).setVisible(true);
            }
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                TelaGabarito frame = new TelaGabarito();
                frame.setVisible(true);
            }
        });
    }
}
