import app.controllers.UsuarioController;
import views.components.PanelUsuarios;

import javax.swing.*;

public class Main {
    private UsuarioController usuarioController;


    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            UsuarioController usuarioController = new UsuarioController();
            PanelUsuarios panelUsuarios = new PanelUsuarios(usuarioController);

            // Adicione o panelUsuarios à janela ou ao container adequado
        });
    }
}
