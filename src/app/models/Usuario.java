package app.models;

import java.io.Serializable;
import java.time.LocalDate;

public class Usuario extends Pessoa implements Serializable {

    private int id;
    private String nomeUsuario = "admin";
    private String senha = "admin";
    private String email;
    private boolean isAdmin;
    private boolean status;


    public Usuario() {
        
    }

    public Usuario(String nome, String nomeUsuario, String senha, String email, boolean isAdmin, boolean status) {
        this.nome = nome;
        this.nomeUsuario = nomeUsuario;
        this.senha = senha;
        this.email = email;
        this.isAdmin = isAdmin;
        this.status = status;


    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNomeUsuario() {
        return nomeUsuario;
    }
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public boolean isAdmin() {
        return isAdmin;
    }
    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }


    // Getters e Setters

    public boolean autenticar(String nomeUsuario, String senha) {
        return this.nomeUsuario.equals(nomeUsuario) && this.senha.equals(senha);
    }


}
