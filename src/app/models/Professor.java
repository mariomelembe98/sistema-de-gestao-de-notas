package app.models;

import app.models.Disciplina;
import app.models.Pessoa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Professor extends Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    private boolean isAdmin;
    private boolean status;

    private List<Disciplina> disciplinas;

    public Professor(String nome, String apelido, String dataNascimento, String sexo, String email, String telefone, String endereco, String nacionalidade, String provincia, boolean status, boolean isAdmin) {
        this.setNome(nome);
        this.setApelido(apelido);
        this.setDataNascimento(dataNascimento);
        this.setSexo(sexo);
        this.setEmail(email);
        this.setTelefone(telefone);
        this.setEndereco(endereco);
        this.setNacionalidade(nacionalidade);
        this.setProvincia(provincia);

    }

    public Professor(String nome, String apelido, Date dataNascimento, String sexo, String email, String telefone, String endereco, String nacionalidade, String provincia, String status, boolean isAdmin) {

    }


    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }
    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }


    public String getNome() {
        return nome;
    }
    public void setNome(Object nome) {
        this.nome = (String) nome;
    }

    public boolean isStatus() {
        return isAdmin;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    private List<Professor> professores = new ArrayList<>();

    public void adicionarProfessor(Professor professor) {
        professores.add(professor);
    }

    public void removerProfessor(int index) {
        professores.remove(index);
    }

    public List<Professor> getProfessores() {
        return professores;
    }

    public boolean getStatus() {
        return status;
    }

    public Object getIsAdmin() {
        return isAdmin;
    }
}
