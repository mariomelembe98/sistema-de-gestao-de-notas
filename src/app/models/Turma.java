package app.models;

import java.util.ArrayList;
import java.util.List;

public class Turma {
    private int id;
    private String turma;
    private String descricao;
    private String ano;
    private String semestre;
    private List<String> professores;
    private String regime;
    private boolean isStatus;

    public Turma() {
        professores = new ArrayList<>();
    }

    public Turma(String turma, String descricao, String ano, String semestre, String[] professores, boolean status, String regime) {
        this.turma = turma;
        this.descricao = descricao;
        this.ano = ano;
        this.semestre = semestre;
        this.professores = new ArrayList<>();
        this.regime = regime;
        this.isStatus = status;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public List<String> getProfessores() {
        return professores;
    }

    public void setProfessores(List<String> professores) {
        this.professores = professores;
    }

    public String getRegime(){
        return regime;
    }

    public void setRegime(String regime){
        this.regime = regime;
    }

    public boolean isStatus() {
        return isStatus;
    }

    public void setStatus(boolean isStatus) {
        this.isStatus = isStatus;
    }


    // Método para formatar a exibição da turma
    @Override
    public String toString() {
        return turma + " - " + descricao;
    }
}
