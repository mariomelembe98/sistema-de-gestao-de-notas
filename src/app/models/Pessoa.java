package app.models;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by USER on 02/05/2024.
 */

public class Pessoa {

    private int id;
    public String nome;
    private String apelido;
    private String dataNascimento;
    private String sexo;
    private String email;
    private String telefone;
    private String endereco;
    private String nacionalidade;
    private String provincia;
    private boolean isAdmin;
    private boolean status;

    public Pessoa() {

    }

    public Pessoa(int id, String nome, String apelido, String nacionalidade, String telefone, String genero, String provincia, LocalDate dataNascimento, boolean isAdmin, boolean status, String email, String fotoPerfil, LocalDate ultimoAcesso) {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }
    public void setApelido(String apelido) {
        this.apelido = apelido;
    }
    public String getDataNascimento() {
        return dataNascimento;
    }
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public String getNacionalidade() {
        return nacionalidade;
    }
    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }
    public String getProvincia() {
        return provincia;
    }
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    public boolean isAdmin() {
        return isAdmin;
    }
    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean statuds) {
        this.status = statuds;
    }



//    public void toStringPessoa(Pessoa pessoa) {
//        System.out.println("ID: " + pessoa.getId());
//        System.out.println("Apelido: " + pessoa.getApelido());
//        System.out.println("Data de nascimento: " + pessoa.getDataNascimento());
//        System.out.println("Sexo: " + pessoa.getSexo());
//        System.out.println("Email: " + pessoa.getEmail());
//        System.out.println("Telefone: " + pessoa.getTelefone());
//        System.out.println("Endereco: " + pessoa.getEndereco());
//        System.out.println("Nacionalidade: " + pessoa.getNacionalidade());
//    }

}
