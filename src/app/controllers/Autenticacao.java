package app.controllers;

import app.models.Usuario;

import java.util.List;

public class Autenticacao {
    public static Usuario autenticar(String nomeUsuario, String senha) {
        List<Usuario> usuarios = UsuarioController.carregarUsuarios();
        for (Usuario usuario : usuarios) {
            if (usuario.getNomeUsuario().equals(nomeUsuario) && usuario.getSenha().equals(senha)) {
                return usuario;
            }
        }
        return null;
    }
}
