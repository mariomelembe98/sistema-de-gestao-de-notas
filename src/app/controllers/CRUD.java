package app.controllers;

import java.util.List;

interface CRUD<T> {
    void adicionar(T item);
    void remover(T item);
    void atualizar(T item);
}

 class Estudante {
    private String nome;
    private double nota1;
    private double nota2;

    public Estudante(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    @Override
    public String toString() {
        return nome;
    }
}

 interface EstudanteCRUD extends CRUD<Estudante> {
    List<Estudante> getList();
}

 class EstudanteCRUDImpl implements EstudanteCRUD {
    private List<Estudante> estudantes;

    @Override
    public void adicionar(Estudante item) {
        estudantes.add(item);
    }

    @Override
    public void remover(Estudante item) {
        estudantes.remove(item);
    }

    @Override
    public void atualizar(Estudante item) {
        // Não é necessário implementar para este exemplo
    }

    @Override
    public List<Estudante> getList() {
        return estudantes;
    }
}
