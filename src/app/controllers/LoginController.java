package app.controllers;

import app.models.Usuario;
import views.TelaLogin;
import views.TelaPrincipal;

import javax.swing.*;

public class LoginController {
    private UsuarioController usuarioController;
    private Usuario usuarioModel;
    private TelaLogin view;

    public LoginController(Usuario usuarioModel) {
        this.usuarioModel = usuarioModel;
    }

    public void setView(TelaLogin view) {
        this.view = view;
    }

    public void realizarLogin() {
        String nomeUsuario = view.getUsuario();
        String senha = view.getSenha();

        // Autenticar o usuário
        if (usuarioModel.autenticar(nomeUsuario, senha)) {
            // Exibir a tela principal
            JOptionPane.showMessageDialog(view, "Bem-vindo, " + usuarioModel.getNome() + "!");
            TelaPrincipal telaPrincipal = new TelaPrincipal(usuarioController);
            telaPrincipal.setVisible(true);
            view.dispose(); // Fecha a tela de login após abrir a tela principal
        } else {
            // Exibir mensagem de erro
            view.exibirMensagemErro("Usuário ou senha incorretos!");
            // Limpar os campos de texto
            view.limparCampos();
        }
    }
}
