package app.controllers;

import app.models.Turma;
import views.components.PanelTurmas;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TurmaController {
    private static final String FILENAME = "turmas.txt";

    public List<Turma> lerTurmas() {
        List<Turma> turmas = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                String turma = data[0];
                String descricao = data[1];
                String ano = data[2];
                String semestre = data[3];
                String[] professores = data[4].split(";");
                String regime = data[5];
                boolean status = Boolean.parseBoolean(data[5]);
                turmas.add(new Turma(turma, descricao, ano, semestre, professores, status, regime));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return turmas;
    }

    public void salvarTurma(Turma turma) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME, true))) {
            bw.write(String.format("%s,%s,%d,%s,%s,%b%n",
                    turma.getTurma(),
                    turma.getDescricao(),
                    turma.getAno(),
                    turma.getSemestre(),
                    String.join(";", turma.getProfessores()),
                    turma.isStatus()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void atualizarArquivo(List<Turma> turmas) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
            for (Turma turma : turmas) {
                bw.write(String.format("%s,%s,%d,%s,%s,%b%n",
                        turma.getTurma(),
                        turma.getDescricao(),
                        turma.getAno(),
                        turma.getSemestre(),
                        String.join(";", turma.getProfessores()),
                        turma.isStatus()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
